package mysql_model

import "time"

// ColumnCreateModifyDeleteTime is a basic model for mysql.
type ColumnCreateModifyDeleteTime struct {
	ID          int64 `gorm:"primary_key;AUTO_INCREMENT" json:"id" db:"id"`
	CreateTime  int64 `json:"create_time" db:"create_time"`
	ModifyTime  int64 `json:"modify_time" db:"modify_time"`
	DeletedTime int64 `json:"deleted_time" db:"deleted_time"`
	IsDel       int32 `json:"is_del" db:"is_del"`
}

type TccBranchTransactionRecord struct {
	ID            int64     `xorm:"primary_key;AUTO_INCREMENT" gorm:"primary_key;AUTO_INCREMENT" json:"id" db:"id"`
	Business      string    `json:"business" db:"business"`
	XID           string    `json:"xid" db:"xid"`
	BID           string    `json:"bid" db:"bid"`
	TryStatus     int8      `json:"try_status" db:"try_status"`
	ConfirmStatus int8      `json:"confirm_status" db:"confirm_status"`
	CancelStatus  int8      `json:"cancel_status" db:"cancel_status"`
	Context       string    `json:"context" db:"context"`
	UpdateTime    time.Time `json:"update_time" db:"update_time"`
	CreateTime    time.Time `json:"create_time" db:"create_time"`
}
