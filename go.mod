module gitee.com/kelvins-io/kelvins

go 1.13

require (
	gitee.com/kelvins-io/common v1.1.7
	gitee.com/kelvins-io/g2cache v4.0.5+incompatible
	github.com/RichardKnop/machinery v1.8.0
	github.com/aliyun/alibaba-cloud-sdk-go v1.61.685 // indirect
	github.com/aliyunmq/mq-http-go-sdk v0.0.0-20190911115909-92078b373925 // indirect
	github.com/astaxie/beego v1.12.3
	github.com/bluele/gcache v0.0.2
	github.com/cloudflare/tableflip v1.2.2
	github.com/coocood/freecache v1.1.1 // indirect
	github.com/etcd-io/etcd v3.3.25+incompatible
	github.com/facebookgo/stack v0.0.0-20160209184415-751773369052 // indirect
	github.com/garyburd/redigo v1.6.4 // indirect
	github.com/gin-contrib/pprof v1.3.0
	github.com/gin-gonic/gin v1.8.0
	github.com/go-sql-driver/mysql v1.6.0
	github.com/gogap/errors v0.0.0-20200228125012-531a6449b28c // indirect
	github.com/gogap/stack v0.0.0-20150131034635-fef68dddd4f8 // indirect
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/gomodule/redigo v2.0.0+incompatible
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.4.2
	github.com/grpc-ecosystem/go-grpc-middleware v1.3.0
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/jinzhu/gorm v1.9.15
	github.com/mohae/deepcopy v0.0.0-20170929034955-c48cc78d4826 // indirect
	github.com/prometheus/client_golang v1.12.2
	github.com/qiniu/qmgo v0.7.0
	github.com/robfig/cron/v3 v3.0.1
	github.com/rs/zerolog v1.20.0 // indirect
	github.com/seata/seata-go v1.2.0
	github.com/valyala/fasthttp v1.17.0 // indirect
	golang.org/x/net v0.5.0
	google.golang.org/grpc v1.40.0
	gopkg.in/ini.v1 v1.51.0
	xorm.io/xorm v1.0.3
)

replace (
	go.opentelemetry.io/ote/api/global => go.opentelemetry.io/ote/api/global v1.21.0
	go.opentelemetry.io/ote/api/trace => go.opentelemetry.io/ote/api/trace v1.21.0
	google.golang.org/grpc => google.golang.org/grpc v1.40.0
)
