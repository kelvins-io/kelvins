package kelvins

const (
	Version = "1.7.1"
)

const (
	RPCMetadataRequestId    = "x-request-id"
	RPCMetadataServiceNode  = "x-service-node"
	RPCMetadataServiceName  = "x-service-name"
	RPCMetadataResponseTime = "x-response-time"
	RPCMetadataHandleTime   = "x-handle-time"
	RPCMetadataPowerBy      = "x-powered-by"
)

const (
	HttpMetadataRequestId    = "X-Request-Id"
	HttpMetadataServiceNode  = "X-Service-Node"
	HttpMetadataServiceName  = "X-Service-Name"
	HttpMetadataResponseTime = "X-Response-Time"
	HttpMetadataHandleTime   = "X-Handle-Time"
	HttpMetadataPowerBy      = "X-Powered-By"
)

const (
	SqlProxySeataXA = "seata-xa"
	SqlProxySeataAT = "seata-at"
)

const (
	ResponseTimeLayout = "2006-01-02 15:04:05"
)
