package config

import (
	"flag"
	"gitee.com/kelvins-io/kelvins"
	"gitee.com/kelvins-io/kelvins/config/setting"
	"gopkg.in/ini.v1"
	"log"
)

const (
	// ConfFileName defines config file name.
	ConfFileName = "./etc/app.ini"
	// SectionServer is a section name for grpc server.
	SectionServer = "kelvins-server"
	// SectionHttpServer is a section name for http
	SectionHttpServer = "kelvins-http-server"
	// SectionHttpRateLimit is a section mame for http
	SectionHttpRateLimit = "kelvins-http-rate-limit"
	// SectionLogger is a section name for logger.
	SectionLogger = "kelvins-logger"
	// SectionMysql is a sectoin name for mysql.
	SectionMysql = "kelvins-mysql"
	// SectionG2cache is a section name for g2cache
	SectionG2cache = "kelvins-g2cache"
	// SectionRedis is a section name for redis
	SectionRedis = "kelvins-redis"
	// SectionMongodb is a section name for mongodb
	SectionMongoDB = "kelvins-mongodb"
	// SectionQueueRedis is a section name for redis queue
	SectionQueueRedis = "kelvins-queue-redis"
	// SectionQueueAliAMQP is a section name for ali amqp
	SectionQueueAliAMQP = "kelvins-queue-ali-amqp"
	// SectionQueueAMQP is a section name for amqp
	SectionQueueAMQP = "kelvins-queue-amqp"
	// SectionQueueAliRocketMQ is a section name for ali-rocketmq
	SectionQueueAliRocketMQ = "kelvins-queue-ali-rocketmq"
	// SectionQueueServer is a section name for queue-server
	SectionQueueServer = "kelvins-queue-server"
	// SectionGPool is goroutine pool
	SectionGPool = "kelvins-gpool"
	// SectionJwt is jwt
	SectionJwt = "kelvins-jwt"
	// SectionAuth is rpc auth ,just for compatibility, it will be deleted in the future
	SectionAuth = "kelvins-auth"
	// SectionRPCAuth is rpc auth
	SectionRPCAuth = "kelvins-rpc-auth"
	// SectionRPCServerParams is server rpc params
	SectionRPCServerParams = "kelvins-rpc-server"
	// SectionRPCServerKeepaliveParams is server rpc keep alive params
	SectionRPCServerKeepaliveParams = "kelvins-rpc-server-kp"
	// SectionRPCServerKeepaliveEnforcementPolicy is server rpc keep alive enf policy
	SectionRPCServerKeepaliveEnforcementPolicy = "kelvins-rpc-server-kep"
	// SectionRPCClientKeepaliveParams is client rpc keep alive params
	SectionRPCClientKeepaliveParams = "kelvins-rpc-client-kp"
	// SectionRPCTransportBuffer is rpc transport buffer
	SectionRPCTransportBuffer = "kelvins-rpc-transport-buffer"
	// SectionRPCRateLimit is rpc rate limit
	SectionRPCRateLimit = "kelvins-rpc-rate-limit"
	// SectionTransactionSeata  is seata support
	SectionTransactionSeata = "kelvins-transaction-seata"
)

// cfg reads file app.ini.
var (
	cfg            *ini.File
	flagConfigPath = flag.String("conf_file", "", "set config file path")
)

// LoadDefaultConfig loads config form cfg.
func LoadDefaultConfig(application *kelvins.Application) error {
	flag.Parse()
	var configFile = ConfFileName
	if *flagConfigPath != "" {
		configFile = *flagConfigPath
	}

	// Setup cfg object
	var err error
	cfg, err = ini.Load(configFile)
	if err != nil {
		return err
	}

	// Setup default settings
	for _, sectionName := range cfg.SectionStrings() {
		defaultConfInitFunc(sectionName)
	}
	return nil
}

func defaultConfInitFunc(sectionName string) {
	var defaultConfInitFuncStore = map[string]func(){
		SectionServer: func() {
			kelvins.ServerSetting = new(setting.ServerSettingS)
			MapConfig(sectionName, kelvins.ServerSetting)
		},
		SectionHttpServer: func() {
			kelvins.HttpServerSetting = new(setting.HttpServerSettingS)
			MapConfig(sectionName, kelvins.HttpServerSetting)
		},
		SectionHttpRateLimit: func() {
			kelvins.HttpRateLimitSetting = new(setting.HttpRateLimitSettingS)
			MapConfig(sectionName, kelvins.HttpRateLimitSetting)
		},
		SectionJwt: func() {
			kelvins.JwtSetting = new(setting.JwtSettingS)
			MapConfig(sectionName, kelvins.JwtSetting)
		},
		SectionAuth: func() {
			kelvins.RPCAuthSetting = new(setting.RPCAuthSettingS)
			MapConfig(sectionName, kelvins.RPCAuthSetting)
		},
		SectionRPCAuth: func() {
			kelvins.RPCAuthSetting = new(setting.RPCAuthSettingS)
			MapConfig(sectionName, kelvins.RPCAuthSetting)
		},
		SectionRPCServerParams: func() {
			kelvins.RPCServerParamsSetting = new(setting.RPCServerParamsS)
			MapConfig(sectionName, kelvins.RPCServerParamsSetting)
		},
		SectionRPCServerKeepaliveParams: func() {
			kelvins.RPCServerKeepaliveParamsSetting = new(setting.RPCServerKeepaliveParamsS)
			MapConfig(sectionName, kelvins.RPCServerKeepaliveParamsSetting)
		},
		SectionRPCServerKeepaliveEnforcementPolicy: func() {
			kelvins.RPCServerKeepaliveEnforcementPolicySetting = new(setting.RPCServerKeepaliveEnforcementPolicyS)
			MapConfig(sectionName, kelvins.RPCServerKeepaliveEnforcementPolicySetting)
		},
		SectionRPCClientKeepaliveParams: func() {
			kelvins.RPCClientKeepaliveParamsSetting = new(setting.RPCClientKeepaliveParamsS)
			MapConfig(sectionName, kelvins.RPCClientKeepaliveParamsSetting)
		},
		SectionRPCTransportBuffer: func() {
			kelvins.RPCTransportBufferSetting = new(setting.RPCTransportBufferS)
			MapConfig(sectionName, kelvins.RPCTransportBufferSetting)
		},
		SectionRPCRateLimit: func() {
			kelvins.RPCRateLimitSetting = new(setting.RPCRateLimitSettingS)
			MapConfig(sectionName, kelvins.RPCRateLimitSetting)
		},
		SectionLogger: func() {
			kelvins.LoggerSetting = new(setting.LoggerSettingS)
			MapConfig(sectionName, kelvins.LoggerSetting)
		},
		SectionMysql: func() {
			kelvins.MysqlSetting = new(setting.MysqlSettingS)
			MapConfig(sectionName, kelvins.MysqlSetting)
		},
		SectionRedis: func() {
			kelvins.RedisSetting = new(setting.RedisSettingS)
			MapConfig(sectionName, kelvins.RedisSetting)
		},
		SectionG2cache: func() {
			kelvins.G2CacheSetting = new(setting.G2CacheSettingS)
			MapConfig(sectionName, kelvins.G2CacheSetting)
		},
		SectionMongoDB: func() {
			kelvins.MongoDBSetting = new(setting.MongoDBSettingS)
			MapConfig(sectionName, kelvins.MongoDBSetting)
		},
		SectionQueueRedis: func() {
			kelvins.QueueRedisSetting = new(setting.QueueRedisSettingS)
			MapConfig(sectionName, kelvins.QueueRedisSetting)
		},
		SectionQueueAliAMQP: func() {
			kelvins.QueueAliAMQPSetting = new(setting.QueueAliAMQPSettingS)
			MapConfig(sectionName, kelvins.QueueAliAMQPSetting)
		},
		SectionQueueAMQP: func() {
			kelvins.QueueAMQPSetting = new(setting.QueueAMQPSettingS)
			MapConfig(sectionName, kelvins.QueueAMQPSetting)
		},
		SectionQueueAliRocketMQ: func() {
			kelvins.AliRocketMQSetting = new(setting.AliRocketMQSettingS)
			MapConfig(sectionName, kelvins.AliRocketMQSetting)
		},
		SectionQueueServer: func() {
			kelvins.QueueServerSetting = new(setting.QueueServerSettingS)
			MapConfig(sectionName, kelvins.QueueServerSetting)
		},
		SectionGPool: func() {
			kelvins.GPoolSetting = new(setting.GPoolSettingS)
			MapConfig(sectionName, kelvins.GPoolSetting)
		},
		SectionTransactionSeata: func() {
			kelvins.TransactionSeataSetting = new(setting.TransactionSeataSettingS)
			MapConfig(sectionName, kelvins.TransactionSeataSetting)
		},
	}
	if v, ok := defaultConfInitFuncStore[sectionName]; ok {
		v()
	}
}

// MapConfig uses cfg to map config.
func MapConfig(section string, v interface{}) {
	log.Printf("[info] Load default config %s", section)
	sec, err := cfg.GetSection(section)
	if err != nil {
		log.Fatalf("[err] Fail to parse '%s': %v", section, err)
	}
	err = sec.MapTo(v)
	if err != nil {
		log.Fatalf("[err] %s section map to setting err: %v", section, err)
	}
}
